#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#define SIZE 1048576

int main(int argc, char **argv)
{
    int fd = shm_open("/test.shm", O_CREAT | O_RDWR, 0777);
    if (fd == -1) {
        perror("shm_open");
        return 1;
    }

    if (ftruncate(fd, SIZE)) {
        perror("ftruncate");
        return 1;
    }

    char *mem = (char*)mmap(NULL, SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (! mem || ((void*)mem) == MAP_FAILED) {
        perror("mmap");
        return 1;
    }

    //memset(mem, 13, SIZE);

    for (int i = 0; i <= SIZE; ++i) {
        char *cell = mem + i;
        *cell = 13;
    }
    
    return 0;
}
